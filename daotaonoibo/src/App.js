import React, { Component } from 'react'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';
import Login from './components/authentic/Login';
import ForgotPassword from './components/authentic/ForgotPassword';
import LoginSuccess from './components/authentic/LoginSuccess';
import Register from './components/authentic/Register';

export default class App extends Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route path="/login-success">
                        <LoginSuccess />
                    </Route>
                    <Route path="/forgot-password">
                        <ForgotPassword />
                    </Route>
                    <Route path="/register">
                        <Register />
                    </Route>
                    <Route path="/">
                        <Login />
                    </Route>
                </Switch>
            </Router>
        )
    }
}

