import React, { Component } from 'react'

export default class Logo extends Component {
    render() {
        return (
            <div className="logo">
                <a href="imap.edu.vn" className="image img-scaledown">
                    <img src="img/hd-logo.png" alt="Logo Imap" />
                </a>
            </div>
        )
    }
}
