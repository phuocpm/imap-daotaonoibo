import React, { Component } from 'react'
import Logo from '../shared/Logo'
import { Link } from 'react-router-dom'

export default class ForgotPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            txtEmail: ''
        }
        this.onHandleChange = this.onHandleChange.bind(this);
        this.onHandleSubmit = this.onHandleSubmit.bind(this);
    }

    onHandleChange(event) {
        var target = event.target;
        var name = target.name;
        var value = target.value;
        this.setState({
            [name]: value
        })
    }

    onHandleSubmit(event) {
        event.preventDefault();
        console.log(this.state);
    }

    render() {
        return (
            <div className="aut-page login-page">
                <div className="container-fluid pl0 pr0">
                    <div className="aut-common">
                        <div className="row row-mg-0">
                            <div className="col-lg-6 col-pd-0">
                                <div className="aut-common-left">
                                    <div className="row justify-content-center align-items-center">
                                        <div className="col-lg-6">
                                            <div className="aut-common-left-content">
                                                <Logo />
                                                <div className="sec-header sec-header-common">
                                                    <div className="heading">Quên mật khẩu</div>
                                                    <div className="sub-heading">password</div>
                                                </div>
                                                <form className="form-aut" onSubmit={this.onHandleSubmit}>
                                                    <div className="form-group">
                                                        <label htmlFor="inputEmail">Email đăng ký</label>
                                                        <input type="email" className="form-control" id="inputEmail1" placeholder="Enter email"
                                                            autoComplete="off"
                                                            name="txtEmail"
                                                            onChange={this.onHandleChange}
                                                        />
                                                    </div>
                                                    <div className="box-button">
                                                        <button type="submit" className="btn btn-danger">Khôi phục</button>
                                                    </div>
                                                    <div className="back-login">
                                                        <Link to="/" className="text text-default">Đăng nhập</Link>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6 col-pd-0">
                                <div className="aut-common-right">
                                    <div className="thumb">
                                        <div className="image img-cover">
                                            <img src="img/bg-reset.jpg" alt="Ms Hoa Toeic" />
                                        </div>
                                    </div>
                                    <div className="overlay" />
                                    <div className="info">
                                        <div className="info-content">
                                            <div className="item-1">
                                                <img src="img/item-1.png" alt="dau ngoac trai"  />
                                            </div>
                                            <div className="item-quote">
                                                Tại IMAP, giáo viên không chỉ dạy kiến thức mà được đào tạo trở thành những
                                                người có ảnh hưởng hay tạo được cảm hứng học tiếng Anh cho học viên, không dùng
                                                tính cách của mình để dạy học viên. Nếu tính cách chưa phù hợp thì các giáo
                                                viên cần phải thay đổi tính cách của mình đủ để IMAP thấy yên tâm khi trao học
                                                viên vào tay họ.
                                            </div>
                                            <div className="item-2">
                                                <img src="img/item-2.png" alt="dau ngoac phai" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
