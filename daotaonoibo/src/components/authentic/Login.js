import React, { Component } from 'react';
import Logo from '../shared/Logo';
import { Link } from 'react-router-dom';


export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            txtEmail: '',
            txtPassword: ''
        }
        this.onHandleChange = this.onHandleChange.bind(this);
        this.onHandleSubmit = this.onHandleSubmit.bind(this);
    }

    onHandleChange(event) {
        var target = event.target;
        var name = target.name;
        var value = target.value;
        this.setState({
            [name]: value
        })
    }

    onHandleSubmit(event) {
        event.preventDefault();
        console.log(this.state);
    }

    render() {
        return (
            <div className="aut-page login-page">
                <div className="container-fluid pl0 pr0">
                    <div className="aut-common">
                        <div className="row row-mg-0">
                            <div className="col-lg-6 col-pd-0">
                                <div className="aut-common-left">
                                    <div className="row justify-content-center align-items-center">
                                        <div className="col-lg-6">
                                            <div className="aut-common-left-content">
                                                <Logo />
                                                <div className="sec-header sec-header-common">
                                                    <div className="heading">Đăng nhập</div>
                                                    <div className="sub-heading">Login</div>
                                                </div>
                                                <form className="form-aut" onSubmit={this.onHandleSubmit}>
                                                    <div className="form-group">
                                                        <label htmlFor="inputEmail">Email</label>
                                                        <input type="email" className="form-control" id="inputEmail1" placeholder="Enter email"
                                                            name="txtEmail"
                                                            onChange={this.onHandleChange}
                                                            autoComplete="off"
                                                        />
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="inputPass">Mật khẩu</label>
                                                        <input type="password" className="form-control" id="inputPass" placeholder="Enter password"
                                                            name="txtPassword"
                                                            onChange={this.onHandleChange}
                                                        />
                                                    </div>
                                                    <div className="box-tool">
                                                        <Link to="/forgot-password" className="text text-left">Quên mật khẩu</Link>
                                                        <Link to="/register" className="text text-right">Đăng ký</Link>
                                                    </div>
                                                    <div className="box-button">
                                                        <button type="submit" className="btn btn-danger">Đăng nhập</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6 col-pd-0">
                                <div className="aut-common-right">
                                    <div className="thumb">
                                        <div className="image img-cover">
                                            <img src="img/bg-login.jpg" alt="Ms Hoa toeic" />
                                        </div>
                                    </div>
                                    <div className="overlay" />
                                    <div className="info">
                                        <div className="info-content">
                                            <div className="item-1">
                                                <img src="img/item-1.png" alt="dau ngoac trai" />
                                            </div>
                                            <div className="item-quote">
                                                Tại IMAP, giáo viên không chỉ dạy kiến thức mà được đào tạo trở thành những
                                                người có ảnh hưởng hay tạo được cảm hứng học tiếng Anh cho học viên, không dùng
                                                tính cách của mình để dạy học viên. Nếu tính cách chưa phù hợp thì các giáo
                                                viên cần phải thay đổi tính cách của mình đủ để IMAP thấy yên tâm khi trao học
                                                viên vào tay họ.
                                            </div>
                                            <div className="item-2">
                                                <img src="img/item-2.png" alt="dau ngoac phai" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}
